/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2019,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		IAR 8.3 or MDK 5.26
 * @Target core		NXP RT1021DAG5A
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2019-02-18
 ********************************************************************************************************************/


//整套推荐IO查看Projecct文件夹下的TXT文本



//打开新的工程或者工程移动了位置务必执行以下操作
//第一步 关闭上面所有打开的文件
//第二步 project  clean  等待下方进度条走完



#include "headfile.h"



        
        
uint8 test_write[256];
uint8 test_read[256];


int main(void)
{
    DisableGlobalIRQ();
    board_init();   //务必保留，本函数用于初始化MPU 时钟 调试串口
    
	//-----------------特别注意-----------------
	//-----------------特别注意-----------------
	//-----------------特别注意-----------------
	
	//需要特别注意本例程操作的flash并不是操作的核心板上的flash芯片
	//而是需要自己外接一个flash芯片，具体接线可查看SEEKFREE_W25Q64.h最上方的备注信息
	//RT1021的库不在提供直接将参数存放在板载flash上的操作
	
	//本例程不提供技术支持，例程测试过没有问题
	//如果使用过程中遇到问题请自行解决
	//-----------------特别注意-----------------
	//-----------------特别注意-----------------
	//-----------------特别注意-----------------
    w25qxx_init();
    test_write[0] = 15;
    test_write[4] = 15;
    w25qxx_erase_sector(1);
    w25qxx_write_page(1,1,test_write,256);
    w25qxx_read_page(1,1,test_read,256);
    
    //通过在线调试直接查看test_read数组的值是否与test_write的值一样即可。
    //值一样表示写入与读取正常，否则操作flash失败
    EnableGlobalIRQ(0);
    while(1)
    {


        
    }
}
